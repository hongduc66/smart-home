import 'core/constants/api_const.dart';
import 'main.dart';

void main() {
  APIConst.setBaseUrl(Environment.production);
  runSmartHomeApp();
}
