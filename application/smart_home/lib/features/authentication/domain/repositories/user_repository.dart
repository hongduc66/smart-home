import 'package:smart_home/core/api/models/api_response.dart';
import 'package:smart_home/features/authentication/domain/entities/user.dart';

abstract class UserRepository {
  Future<ApiResponse<User?>> verifyUser({required String accessToken});
}
