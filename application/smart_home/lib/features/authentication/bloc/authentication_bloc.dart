import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:smart_home/features/authentication/data/rest_api/authentication_repository_impl.dart';
import 'package:smart_home/features/authentication/data/rest_api/user_repository_impl.dart';
import 'package:smart_home/features/authentication/data/shared_preferences/authentication_storage.dart';
import 'package:smart_home/features/authentication/domain/entities/user.dart';

part 'authentication_state.dart';
part 'authentication_event.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc({
    required RestApiAuthenticationRepository authenticationRepository,
    required RestApiUserRepository userRepository,
  })  : _authenticationRepository = authenticationRepository,
        _userRepository = userRepository,
        super(
          const AuthenticationState.unknown(),
        ) {
    on<AuthenticationLogin>(_onAuthenticationLogin);
    on<AuthenticationSignup>(_onAuthenticationSignup);
    on<AuthenticationLogout>(_onAuthenticationLogout);
    on<AuthenticationAutoLogin>(_onAuthenticationAutoLogin);

    add(AuthenticationAutoLogin());
  }

  final RestApiAuthenticationRepository _authenticationRepository;
  final RestApiUserRepository _userRepository;

  Future<void> _onAuthenticationLogin(
    AuthenticationLogin event,
    Emitter<AuthenticationState> emit,
  ) async {
    try {
      emit(const AuthenticationState.loading());
      var loginResponse = await _authenticationRepository.logIn(
        username: event.username,
        password: event.password,
      );
      if (loginResponse.isFailed) {
        emit(AuthenticationState.failed(loginResponse.error!.message));
      } else {
        final accessToken = loginResponse.result!.data!.accessToken!;
        final user = await _tryGetUser(accessToken: accessToken);

        if (user != null) {
          await AuthenticationStorage.updateToken(accessToken);
          await AuthenticationStorage.updateCredentials(
            username: event.username,
            password: event.password,
          );
          return emit(AuthenticationState.authenticated(user));
        } else {
          await AuthenticationStorage.deleteCredentials();
          return emit(const AuthenticationState.failed('Verify user failed!'));
        }
      }
    } catch (exception) {
      return;
    }
  }

  Future<void> _onAuthenticationSignup(
    AuthenticationSignup event,
    Emitter<AuthenticationState> emit,
  ) async {
    try {
      emit(const AuthenticationState.loading());
      var signupResponse = await _authenticationRepository.signUp(
        email: event.email,
        username: event.username,
        password: event.password,
      );
      if (signupResponse.isFailed) {
        emit(AuthenticationState.failed(signupResponse.error!.message));
      } else {
        var user = signupResponse.result!.data;
        AuthenticationStorage.updateCredentials(
          username: event.username,
        );
        return emit(user != null
            ? AuthenticationState.authenticated(user)
            : const AuthenticationState.failed('Register failed !'));
      }
    } catch (exception) {
      return;
    }
  }

  Future<User?> _tryGetUser({required String accessToken}) async {
    try {
      var getUserResult =
          await _userRepository.verifyUser(accessToken: accessToken);
      return getUserResult.result!.data;
    } catch (_) {
      return null;
    }
  }

  Future<void> _onAuthenticationLogout(
    AuthenticationLogout event,
    Emitter<AuthenticationState> emit,
  ) async {
    await AuthenticationStorage.deleteCredentials();
    return emit(const AuthenticationState.unauthenticated());
  }

  Future<void> _onAuthenticationAutoLogin(
    AuthenticationAutoLogin event,
    Emitter<AuthenticationState> emit,
  ) async {
    final accessToken = AuthenticationStorage.token;
    if (accessToken != null) {
      final user = await _tryGetUser(accessToken: accessToken);
      if (user != null) {
        return emit(AuthenticationState.authenticated(user));
      } else {
        await AuthenticationStorage.deleteCredentials();
        return emit(
          const AuthenticationState.failed(
            'Bad credentials, try log in again !',
          ),
        );
      }
    } else {
      emit(const AuthenticationState.unauthenticated());
    }
  }
}
