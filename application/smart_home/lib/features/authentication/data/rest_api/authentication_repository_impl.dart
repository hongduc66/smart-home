import 'dart:async';
import 'package:get_it/get_it.dart';
import 'package:smart_home/core/api/dio/dio_http_client.dart';
import 'package:smart_home/core/api/models/api_response.dart';
import 'package:smart_home/core/api/models/api_result.dart';
import 'package:smart_home/features/authentication/domain/entities/auth.dart';
import 'package:smart_home/features/authentication/domain/entities/user.dart';
import 'package:smart_home/features/authentication/domain/repositories/authentication_repository.dart';

final GetIt sl = GetIt.instance;

class RestApiAuthenticationRepository implements AuthenticationRepository {
  final DioHttpClient _client = sl.get<DioHttpClient>();

  @override
  Future<ApiResponse<Auth>> logIn({
    required String username,
    required String password,
  }) async {
    try {
      await Future.delayed(const Duration(seconds: 1));
      var response = await _client.postMethod('/authenticate', data: {
        "username": username,
        "password": password,
      });
      return ApiResponse.withResult(
        response: response.data,
        resultConverter: (json) => ApiResultSingle(
          json: json,
          rootName: '',
          jsonConverter: (json) => Auth.fromJson(json),
        ),
      );
    } catch (exception) {
      return ApiResponse.withError(exception);
    }
  }

  @override
  Future<ApiResponse<User>> signUp({
    required String email,
    required String username,
    required String password,
  }) async {
    try {
      var response = await _client.postMethod('/register', data: {
        "email": email,
        "username": username,
        "password": password,
      });

      return ApiResponse.withResult(
        response: response.data,
        resultConverter: (json) => ApiResultSingle(
          json: json,
          rootName: '',
          jsonConverter: (json) => User.fromJson(json),
        ),
      );
    } catch (exception) {
      return ApiResponse.withError(exception);
    }
  }
}
