import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smart_home/core/common_widgets/password_form_field.dart';
import 'package:smart_home/core/common_widgets/text_form_field.dart';
import 'package:smart_home/core/common_widgets/widget_button.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/core/utils/snackbar.dart';
import 'package:smart_home/core/utils/validator.dart';
import 'package:smart_home/features/authentication/bloc/authentication_bloc.dart';
import 'package:smart_home/features/authentication/data/shared_preferences/authentication_storage.dart';
import 'package:smart_home/router.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  final spacerSmall = SizedBox(height: 0.02.sh);
  final spacerMedium = SizedBox(height: 0.04.sh);
  final spacerBig = SizedBox(height: 0.06.sh);

  String? username = AuthenticationStorage.username;
  String? password;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Image(
              width: 0.35.sw,
              image: const AssetImage('lib/asset/splash_icon.png'),
            ),
            spacerBig,
            _usernameInput(),
            spacerSmall,
            _passwordInput(),
            spacerMedium,
            _loginButton(),
            SizedBox(height: 0.01.sh),
            _registerButton(),
          ],
        ),
      ),
    );
  }

  Widget _usernameInput() {
    return TextFormFieldSH(
      labelText: 'Username',
      hintText: 'Enter username...',
      defaultText: username,
      prefixIcon: Icon(
        Icons.account_circle_sharp,
        size: 24.sp,
        color: ColorConst.tealEnhanced,
      ),
      onChanged: (value) => username = value,
      validator: (value) {
        if (!Validator.isValidUsername(value!)) {
          return 'Username is invalid';
        }
      },
    );
  }

  Widget _passwordInput() {
    return PasswordFormFieldSH(
      labelText: 'Password',
      hintText: 'Enter password...',
      prefixIcon: Icon(
        Icons.verified_user,
        size: 24.sp,
        color: ColorConst.tealEnhanced,
      ),
      onChanged: (value) => password = value,
      validator: (value) {
        if (!Validator.isValidPassword(value!)) {
          return 'Password is invalid';
        }
      },
    );
  }

  Widget _loginButton() {
    return WidgetButtonSH(
      height: 40.h,
      onTap: () {
        FocusScope.of(context).unfocus();
        if (_formKey.currentState!.validate()) {
          context.read<AuthenticationBloc>().add(AuthenticationLogin(
                username: username!,
                password: password!,
              ));
        } else {
          showSnackbarSH(
            context,
            'Invalid input, try again!',
            AlertType.error,
          );
        }
      },
      child: Text(
        'Login',
        style: TextStyle(
          color: ColorConst.tealEnhanced,
          fontSize: 18.sp,
          fontWeight: FontWeight.w800,
        ),
      ),
    );
  }

  Widget _registerButton() {
    return TextButton(
      onPressed: () {
        Navigator.pushReplacementNamed(context, AppRouter.register);
      },
      child: Text(
        'Create account',
        style: TextStyle(
          color: ColorConst.tealEnhanced,
          fontSize: 18.sp,
          fontWeight: FontWeight.w800,
          decoration: TextDecoration.underline,
        ),
      ),
    );
  }
}
