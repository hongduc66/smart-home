import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/core/utils/snackbar.dart';
import 'package:smart_home/features/authentication/bloc/authentication_bloc.dart';
import 'package:smart_home/router.dart';

GetIt sl = GetIt.instance();

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        switch (state.status) {
          case AuthenticationStatus.authenticated:
            Navigator.pushReplacementNamed(context, AppRouter.homePage);
            break;
          case AuthenticationStatus.failed:
            showSnackbarSH(context, state.errorMessage!, AlertType.warning);
            Navigator.pushReplacementNamed(context, AppRouter.login);
            break;
          default:
            Navigator.pushReplacementNamed(context, AppRouter.login);
            break;
        }
      },
      child: Scaffold(
        backgroundColor: ColorConst.blueFaded,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                width: MediaQuery.of(context).size.width * 0.35,
                image: const AssetImage('lib/asset/splash_icon.png'),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: const LinearProgressIndicator(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
