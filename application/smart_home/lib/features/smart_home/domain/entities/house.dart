import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'device.dart';

part 'house.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class House extends Equatable {
  final int id;
  final String name;
  final String code;
  final List<Device> sensors;

  House({
    required this.id,
    required this.name,
    required this.code,
    required this.sensors,
  });

  @override
  List<Object?> get props => [id, name, code, sensors];

  factory House.fromJson(Map<String, dynamic> json) => _$HouseFromJson(json);
  Map<String, dynamic> toJson() => _$HouseToJson(this);
}
