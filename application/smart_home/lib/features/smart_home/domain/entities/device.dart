import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'device.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class Device extends Equatable {
  final int idx;
  final int id;
  final String name;
  final String type;

  // final List<House> authorities; ( Owned houses )

  const Device({
    required this.idx,
    required this.id,
    required this.name,
    required this.type,
  });

  @override
  List<Object?> get props => [idx, id, name, type];

  factory Device.fromJson(Map<String, dynamic> json) => _$DeviceFromJson(json);
  Map<String, dynamic> toJson() => _$DeviceToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.none)
class DeviceData extends Equatable {
  final int? houseSensorId;
  final String? sensorType;
  final bool? status;
  final int? temp;
  final double? hum;
  final double? gasConcentration;

  const DeviceData({
    this.houseSensorId,
    this.sensorType,
    this.status,
    this.temp,
    this.hum,
    this.gasConcentration,
  });

  @override
  List<Object?> get props =>
      [houseSensorId, sensorType, status, temp, hum, gasConcentration];

  factory DeviceData.fromJson(Map<String, dynamic> json) =>
      _$DeviceDataFromJson(json);
  Map<String, dynamic> toJson() => _$DeviceDataToJson(this);
}

enum DeviceType { led, thermometer, humidity, gasConcentration, door }

class DeviceTypeName {
  static const String led = "LED";
  static const String thermometer = "THERMOMETER";
  static const String humidity = "HUMIDITY";
  static const String gasConcentration = "GAS_CONCENTRATION";
  static const String door = "DOOR";
}
