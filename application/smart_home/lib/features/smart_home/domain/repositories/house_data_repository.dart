import 'package:smart_home/core/api/models/api_response.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';

abstract class HouseDataRepository {
  /// Will return devices data
  Future<ApiResponse<Map<String, List<DeviceData>>>> fetchHouseData(
      int houseId);
  Future<bool> switchDevice({
    required Device device,
    required dynamic data,
  });
}
