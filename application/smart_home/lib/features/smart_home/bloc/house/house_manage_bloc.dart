import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:smart_home/features/smart_home/data/rest_api/house_repository_impl.dart';
import 'package:smart_home/features/smart_home/domain/entities/house.dart';

part 'house_manage_event.dart';
part 'house_manage_state.dart';

GetIt sl = GetIt.instance;

class HouseManageBloc extends Bloc<HouseManageEvent, HouseManageState> {
  final RestApiHouseRepository _houseRepository;

  HouseManageBloc()
      : _houseRepository = sl.get<RestApiHouseRepository>(),
        super(const HouseManageState.unknown()) {
    on<FetchHouses>(_onFetchHouses);
    on<RegisterHouse>(_onRegisterHouse);
  }

  Future<void> _onFetchHouses(
    FetchHouses event,
    Emitter<HouseManageState> emit,
  ) async {
    emit(const HouseManageState.fetching());
    var getHousesReponse = await _houseRepository.fetchHouses();
    if (getHousesReponse.isFailed) {
      emit(HouseManageState.failed(
          "Fetch data failed !\nError: ${getHousesReponse.error?.message}"));
    } else {
      emit(
          HouseManageState.fetchSuccess(houses: getHousesReponse.result!.data));
    }
  }

  Future<void> _onRegisterHouse(
    RegisterHouse event,
    Emitter<HouseManageState> emit,
  ) async {
    emit(const HouseManageState.registering());
    var registerHouseResponse = await _houseRepository.registerHouse(
      name: event.name,
      devices: event.devices,
    );
    if (registerHouseResponse.isFailed) {
      emit(HouseManageState.failed(
          "Register failed !\nError: ${registerHouseResponse.error?.message}"));
    } else {
      emit(const HouseManageState.registerSuccess());
    }
  }
}
