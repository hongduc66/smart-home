part of 'house_manage_bloc.dart';

class HouseManageState extends Equatable {
  final HouseManageStatus status;
  final List<House>? houses;
  final String? errorMessage;

  const HouseManageState._({
    this.status = HouseManageStatus.unknown,
    this.houses = const [],
    this.errorMessage,
  });

  const HouseManageState.unknown() : this._();
  const HouseManageState.fetching()
      : this._(status: HouseManageStatus.fetching);
  const HouseManageState.registering()
      : this._(status: HouseManageStatus.registering);

  const HouseManageState.fetchSuccess({List<House>? houses})
      : this._(status: HouseManageStatus.fetchSuccess, houses: houses);
  const HouseManageState.registerSuccess()
      : this._(status: HouseManageStatus.registerSuccess);
  const HouseManageState.failed(String errorMessage)
      : this._(status: HouseManageStatus.failed, errorMessage: errorMessage);

  @override
  List<Object?> get props => [status, houses, errorMessage];
}

enum HouseManageStatus {
  unknown,
  fetching,
  registering,
  fetchSuccess,
  registerSuccess,
  failed,
}
