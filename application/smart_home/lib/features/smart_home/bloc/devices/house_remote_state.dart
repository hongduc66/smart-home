part of 'house_remote_bloc.dart';

class HouseRemoteState extends Equatable {
  final HouseRemoteStatus status;
  final Map<String, List<DeviceData?>>? sensorsData;
  final List<DeviceData?> ledsData;
  final String? message;
  final String? requestDeviceType;

  const HouseRemoteState._({
    this.status = HouseRemoteStatus.unknown,
    this.sensorsData = const {},
    this.ledsData = const [],
    this.message,
    this.requestDeviceType,
  });

  const HouseRemoteState.unknown() : this._();
  const HouseRemoteState.fetching()
      : this._(
          status: HouseRemoteStatus.fetching,
        );
  const HouseRemoteState.requesting()
      : this._(
          status: HouseRemoteStatus.requesting,
        );
  const HouseRemoteState.fetchSuccess({
    Map<String, List<DeviceData>>? fetchData,
    String? message,
    String? requestDeviceType,
  }) : this._(
          status: HouseRemoteStatus.fetchSuccess,
          sensorsData: fetchData,
          message: message,
          requestDeviceType: requestDeviceType,
        );
  const HouseRemoteState.failed({
    required String error,
    String? requestDeviceType,
  }) : this._(
          status: HouseRemoteStatus.failed,
          message: error,
          requestDeviceType: requestDeviceType,
        );

  @override
  List<Object?> get props => [status, sensorsData, ledsData];
}

enum HouseRemoteStatus {
  unknown,
  fetching,
  requesting,
  fetchSuccess,
  failed,
}
