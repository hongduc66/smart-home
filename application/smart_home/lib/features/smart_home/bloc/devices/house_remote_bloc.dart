import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:equatable/equatable.dart';
import 'package:smart_home/features/smart_home/data/rest_api/house_data_repository_impl.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';

part 'house_remote_event.dart';
part 'house_remote_state.dart';

GetIt sl = GetIt.instance;

class HouseRemoteBloc extends Bloc<HouseRemoteEvent, HouseRemoteState> {
  final RestApiHouseDataRepository _houseDataRepository;
  late int currentHouseId;
  HouseRemoteBloc()
      : _houseDataRepository = sl.get<RestApiHouseDataRepository>(),
        super(const HouseRemoteState.unknown()) {
    on<FetchHouseData>(_onFetchHouseData);
    on<SwitchDevice>(_onSwitchDevice);
  }

  Future<void> _onFetchHouseData(
    FetchHouseData event,
    Emitter<HouseRemoteState> emitter,
  ) async {
    emitter(const HouseRemoteState.fetching());
    await Future.delayed(const Duration(seconds: 1));
    var houseData = await _houseDataRepository.fetchHouseData(event.houseId);
    currentHouseId = event.houseId;
    emitter(
      HouseRemoteState.fetchSuccess(
        fetchData: houseData.result!.data,
      ),
    );
  }

  Future<void> _onSwitchDevice(
    SwitchDevice event,
    Emitter<HouseRemoteState> emitter,
  ) async {
    emitter(const HouseRemoteState.fetching());
    bool requestResult = await _houseDataRepository.switchDevice(
      device: event.device,
      data: event.data,
    );

    String? errorMessage;
    String? successMessage;
    switch (event.device.type) {
      case DeviceTypeName.led:
        errorMessage =
            'Request to turn ${event.device.name} #${event.device.idx} ${event.data ? 'ON' : 'OFF'} has failed !';
        successMessage =
            '${event.device.name} #${event.device.idx} has been turn ${event.data ? 'ON' : 'OFF'}!';
        break;
      case DeviceTypeName.door:
        errorMessage =
            'Request to ${event.data ? 'OPEN' : 'CLOSE'} ${event.device.name} #${event.device.idx} has failed !';
        successMessage =
            '${event.device.name} #${event.device.idx} was ${event.data ? 'OPENED' : 'CLOSED'}!';
        break;
      case DeviceTypeName.thermometer:
        errorMessage =
            'Request to change ${event.device.name} #${event.device.idx} temperature has failed !';
        successMessage =
            'Room of ${event.device.name} #${event.device.idx} is at ${event.data}ºC now';
        break;

      default:
        break;
    }

    if (requestResult == false) {
      // Error while trying to connect to Broker
      emitter(HouseRemoteState.failed(
          error: errorMessage!, requestDeviceType: event.device.type));
    } else {
      emitter(HouseRemoteState.fetchSuccess(
          message: successMessage, requestDeviceType: event.device.type));
      add(FetchHouseData(houseId: currentHouseId));
    }
  }
}
