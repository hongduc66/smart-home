import 'package:get_it/get_it.dart';
import 'package:smart_home/core/api/dio/dio_http_client.dart';
import 'package:smart_home/core/api/iot/amqp_client.dart';
import 'package:smart_home/core/api/models/api_result.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';
import 'package:smart_home/core/api/models/api_response.dart';
import 'package:smart_home/features/smart_home/domain/repositories/house_data_repository.dart';

final GetIt sl = GetIt.instance;

class RestApiHouseDataRepository implements HouseDataRepository {
  final DioHttpClient _client = sl.get<DioHttpClient>();
  final AmqpClient _iotClient = sl.get<AmqpClient>();

  @override
  Future<ApiResponse<Map<String, List<DeviceData>>>> fetchHouseData(
    int houseId,
  ) async {
    try {
      var sensorData =
          await _client.postMethod('/sensor-data-house/mobile/$houseId');
      return ApiResponse.withResult(
        response: sensorData.data,
        resultConverter: (json) => ApiResultSingle(
            json: json,
            rootName: '',
            jsonConverter: (json) {
              Map<String, List<DeviceData>> data = {};
              var humidityDataList = json['HUMIDITY'] as List;
              var thermometerDataList = json['THERMOMETER'] as List;
              var gasConcentrationDataList = json['GAS_CONCENTRATION'] as List;
              var ledList = json['LED'] as List;
              var doorList = json['DOOR'] as List;

              List<DeviceData>? humidityData = humidityDataList
                  .map((data) => DeviceData.fromJson(data))
                  .toList();
              List<DeviceData>? thermometerData = thermometerDataList
                  .map((data) => DeviceData.fromJson(data))
                  .toList();
              List<DeviceData>? gasConcentrationData = gasConcentrationDataList
                  .map((data) => DeviceData.fromJson(data))
                  .toList();
              List<DeviceData>? ledData =
                  ledList.map((data) => DeviceData.fromJson(data)).toList();
              List<DeviceData>? doorData =
                  doorList.map((data) => DeviceData.fromJson(data)).toList();

              data['HUMIDITY'] = humidityData;
              data['THERMOMETER'] = thermometerData;
              data['GAS_CONCENTRATION'] = gasConcentrationData;
              data['LED'] = ledData;
              data['DOOR'] = doorData;
              return data;
            }),
      );
    } catch (exception) {
      print('Error occurs while fetching house data: $exception');
      return ApiResponse.withError(exception);
    }
  }

  @override
  Future<bool> switchDevice({
    required Device device,
    required dynamic data,
  }) async {
    String fieldName = '';
    switch (device.type) {
      case DeviceTypeName.led:
        fieldName = 'status';
        break;
      case DeviceTypeName.door:
        fieldName = 'status';
        break;
      case DeviceTypeName.thermometer:
        fieldName = 'temp';
        break;
      default:
        break;
    }

    return await _iotClient.switchDevice(
      device: device,
      fieldName: fieldName,
      data: data,
    );
  }
}
