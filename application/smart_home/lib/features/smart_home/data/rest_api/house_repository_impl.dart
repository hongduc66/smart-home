import 'package:get_it/get_it.dart';
import 'package:smart_home/core/api/dio/dio_http_client.dart';
import 'package:smart_home/core/api/models/api_result.dart';
import 'package:smart_home/features/smart_home/domain/entities/house.dart';
import 'package:smart_home/core/api/models/api_response.dart';
import 'package:smart_home/features/smart_home/domain/repositories/house_repository.dart';

final GetIt sl = GetIt.instance;

class RestApiHouseRepository implements HouseRepository {
  final DioHttpClient _client = sl.get<DioHttpClient>();

  @override
  Future<ApiResponse<List<House>?>> fetchHouses() async {
    try {
      await Future.delayed(const Duration(seconds: 1));
      var response = await _client.getMethod('/houses', params: {});
      return ApiResponse.withResultAsList(
        response: response.data,
        resultConverter: (list) => ApiResultAsList(
          list: list,
          jsonConverter: (json) => House.fromJson(json),
        ),
      );
    } catch (exception) {
      return ApiResponse.withError(exception);
    }
  }

  @override
  Future<ApiResponse<dynamic>> registerHouse({
    required String name,
    required List<Map<String, dynamic>> devices,
  }) async {
    try {
      await _client.postMethod('/houses', data: {
        "name": name,
        "sensors": devices,
      });
      return ApiResponse.success();
    } catch (exception) {
      return ApiResponse.withError(exception);
    }
  }
}
