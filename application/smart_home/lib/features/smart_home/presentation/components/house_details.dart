import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';
import 'package:smart_home/features/smart_home/domain/entities/house.dart';
import 'package:smart_home/features/smart_home/presentation/utils/style_constant.dart';

void showHouseDetails({
  required BuildContext context,
  required House house,
}) {
  showDialog(
    context: context,
    builder: (context) {
      return Dialog(
        elevation: 8.h,
        child: Padding(
          padding: EdgeInsets.all(16.r),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('House #${house.id}', style: kDashboardTitleStyle),
              SizedBox(height: 12.h),
              detailsRow('Name', house.name),
              SizedBox(height: 4.h),
              detailsRow('Code', house.code),
              SizedBox(height: 12.h),
              Text(
                'Devices ( ${house.sensors.length} )',
                style: kDashboardTitleStyle,
              ),
              tableRow(style: kHouseDetailsStyle),
              SizedBox(
                height: 0.5.sw,
                child: ListView.builder(
                  itemCount: house.sensors.length,
                  itemBuilder: (context, index) => tableRow(
                    device: house.sensors[index],
                  ),
                  physics: const BouncingScrollPhysics(),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}

Widget detailsRow(String title, String details) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        title,
        style: kHouseDetailsStyle.copyWith(
          fontWeight: FontWeight.w300,
        ),
      ),
      Text(
        details,
        style: kHouseDetailsStyle,
      ),
    ],
  );
}

Widget tableRow({
  Device? device,
  TextStyle? style,
}) {
  return Container(
    margin: EdgeInsets.only(bottom: 4.r),
    padding: EdgeInsets.all(4.r),
    child: Row(
      children: [
        Expanded(
          flex: 1,
          child: Text(
            device != null ? device.idx.toString() : 'ID',
            style: style ?? kDetailsRowStyle,
          ),
        ),
        Expanded(
          flex: 3,
          child: Align(
            alignment: Alignment.centerLeft,
            child: FittedBox(
              fit: BoxFit.cover,
              child: Text(
                device != null ? device.name : 'Model',
                style: style ?? kDetailsRowStyle,
              ),
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Align(
            alignment: Alignment.centerRight,
            child: FittedBox(
              fit: BoxFit.cover,
              child: Text(
                device != null ? device.type : 'Type',
                style: style ?? kDetailsRowStyle,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
