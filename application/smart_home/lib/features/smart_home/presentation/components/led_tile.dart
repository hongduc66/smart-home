import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smart_home/core/common_widgets/widget_button.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/core/utils/snackbar.dart';
import 'package:smart_home/features/smart_home/bloc/devices/house_remote_bloc.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';

import '../utils/style_constant.dart';

class LedTile extends StatelessWidget {
  final Device led;
  const LedTile({
    Key? key,
    required this.led,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double widgetHeight = 115.h;
    double widgetWidth = 336.w;
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        Container(
          height: widgetHeight,
          margin: kLedTileMargin,
          decoration: BoxDecoration(
            gradient: ColorConst.softFeeling,
            image: DecorationImage(
              image: AssetImage('lib/asset/led_${led.id}.jpg'),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(12.0),
            boxShadow: [
              BoxShadow(
                offset: const Offset(3, 3),
                blurRadius: 1.5,
                color: ColorConst.tealEnhanced.withOpacity(0.4),
              ),
            ],
          ),
          child: Padding(
            padding: EdgeInsets.only(right: 8.w),
            child: Align(
              alignment: Alignment.bottomRight,
              child: BlocConsumer<HouseRemoteBloc, HouseRemoteState>(
                listener: (context, state) {
                  if (state.message == null ||
                      state.requestDeviceType != DeviceTypeName.led) return;
                  if (state.status == HouseRemoteStatus.fetchSuccess) {
                    showSnackbarSH(context, state.message!, AlertType.success);
                  } else if (state.status == HouseRemoteStatus.failed) {
                    showSnackbarSH(context, state.message!, AlertType.error);
                  }
                },
                builder: (context, state) {
                  if (state.status == HouseRemoteStatus.fetching ||
                      state.status == HouseRemoteStatus.requesting) {
                    return const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: CircularProgressIndicator(),
                    );
                  } else if (state.status == HouseRemoteStatus.fetchSuccess) {
                    DeviceData? data = state.sensorsData!['LED']?.firstWhere(
                      (l) => l?.houseSensorId == led.idx,
                      orElse: () => const DeviceData(),
                    );
                    return buildLedStatusWidget(data, context);
                  } else {
                    return buildLedStatusWidget(null, null);
                  }
                },
              ),
            ),
          ),
        ),
        Positioned(
          top: widgetHeight / 8,
          left: widgetWidth / 12,
          child: Text(
            led.name,
            style: kLedTileTitleStyle,
          ),
        ),
        Positioned(
          bottom: widgetHeight / 8 * 3,
          left: widgetWidth / 12,
          child: Row(
            children: [
              const Icon(
                Icons.qr_code_outlined,
                color: Colors.white,
              ),
              const SizedBox(width: 6.0),
              Text(
                'ID ${led.idx}',
                style: kLedTileSubTitleStyle,
              ),
            ],
          ),
        ),
        Positioned(
          bottom: widgetHeight / 8,
          left: widgetWidth / 12,
          child: Row(
            children: [
              const Icon(
                Icons.device_hub,
                color: Colors.white,
              ),
              const SizedBox(width: 6.0),
              Text(
                'Model #${led.id}',
                style: kLedTileSubTitleStyle,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildLedStatusWidget(
    DeviceData? data,
    BuildContext? context,
  ) {
    Color statusColor;
    String status;
    bool tapable = true;

    if (data == null) {
      statusColor = Colors.amber;
      status = 'Error';
      tapable = false;
    } else if (data.status == null) {
      statusColor = Colors.amber;
      status = 'Unknown';
    } else {
      statusColor = data.status! ? Colors.green : Colors.redAccent;
      status = data.status! ? 'On' : 'Off';
    }

    return Padding(
      padding: EdgeInsets.only(bottom: 12.r, right: 4.r),
      child: WidgetButtonSH(
        backgroundColor: statusColor,
        height: 30.h,
        child: Text(
          'Status: $status',
          style: kLedTileSubTitleStyle,
        ),
        onTap: () {
          if (tapable == false) return;
          BlocProvider.of<HouseRemoteBloc>(context!).add(
            SwitchDevice(
              device: led,
              data: !(data?.status ?? false),
            ),
          );
        },
      ),
    );
  }
}
