import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smart_home/core/common_widgets/dropdown_button.dart';
import 'package:smart_home/core/common_widgets/widget_button.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/features/smart_home/bloc/devices/house_remote_bloc.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';
import 'package:smart_home/features/smart_home/presentation/utils/style_constant.dart';

void showAirConditionerRemote(
  BuildContext context,
  List<Device> listThermometers,
  List<DeviceData?>? listData,
) {
  showModalBottomSheet(
    isDismissible: false,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(30.w),
      ),
    ),
    context: context,
    builder: (context) => AirConditioner(
      listThermometers: listThermometers,
      listData: listData,
    ),
  );
}

class AirConditioner extends StatefulWidget {
  const AirConditioner({
    Key? key,
    required this.listThermometers,
    required this.listData,
  }) : super(key: key);
  final List<Device> listThermometers;
  final List<DeviceData?>? listData;

  @override
  _AirConditionerState createState() => _AirConditionerState();
}

class _AirConditionerState extends State<AirConditioner> {
  Device? selectedThermometer;
  int? currentTemperature;
  int? changedTemperature;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15.r),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Air Conditioner',
              style: kSensorDetailTitleStyle,
            ),
            SizedBox(height: 20.h),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    'Location',
                    style: kDetailsRowStyle,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: DropdownButtonSH(
                    label: 'Location',
                    listDropdownItems: List.generate(
                      widget.listThermometers.length,
                      (index) => buildDropdownItem(
                        value: widget.listThermometers[index].idx,
                        title:
                            'Room of ${widget.listThermometers[index].name} #${widget.listThermometers[index].idx}',
                      ),
                    ),
                    onChanged: (idx) => setState(
                      () {
                        selectedThermometer = widget.listThermometers
                            .firstWhere((device) => device.idx == idx);
                        if (selectedThermometer != null) {
                          currentTemperature = widget.listData!
                              .firstWhere(
                                (data) =>
                                    data?.houseSensorId ==
                                    selectedThermometer!.idx,
                                orElse: () => const DeviceData(),
                              )
                              ?.temp;
                          changedTemperature = currentTemperature;
                        }
                      },
                    ),
                    validator: (_) {},
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.h),
            Container(
              width: 200.r,
              height: 200.r,
              decoration: BoxDecoration(
                color: const Color(0xFFF1F2F6),
                borderRadius: BorderRadius.circular(100.r),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0xFFDADFF0),
                    offset: Offset(10.r, 10.r),
                    blurRadius: 10.r,
                  ),
                ],
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: IconButton(
                      splashColor: Colors.transparent,
                      icon: const Icon(
                        Icons.keyboard_arrow_up,
                        color: ColorConst.tealEnhanced,
                      ),
                      onPressed: () {
                        if (changedTemperature == null) {
                          if (selectedThermometer == null) return;
                          currentTemperature = 0;
                          changedTemperature = 0;
                        }
                        setState(
                          () => changedTemperature = changedTemperature! + 1,
                        );
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '${changedTemperature ?? 'No Data'}',
                          style: kDashboardTitleStyle.copyWith(
                            fontSize: 40.sp,
                          ),
                        ),
                        Text(
                          selectedThermometer == null
                              ? 'Select Location'
                              : 'Room of ${selectedThermometer!.name} #${selectedThermometer!.idx}',
                          style: kDetailsRowStyle,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: IconButton(
                      splashColor: Colors.transparent,
                      icon: const Icon(
                        Icons.keyboard_arrow_down,
                        color: ColorConst.tealEnhanced,
                      ),
                      onPressed: () {
                        if (changedTemperature == null) {
                          if (selectedThermometer == null) return;
                          currentTemperature = 0;
                          changedTemperature = 0;
                        }
                        setState(
                          () => changedTemperature = changedTemperature! - 1,
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.h),
            WidgetButtonSH(
              height: 35.h,
              onTap: () {
                if (currentTemperature == changedTemperature) return;
                Navigator.of(context).pop();
                BlocProvider.of<HouseRemoteBloc>(context).add(
                  SwitchDevice(
                    device: selectedThermometer!,
                    data: changedTemperature,
                  ),
                );
              },
              child: Opacity(
                opacity: currentTemperature == changedTemperature ? 0.5 : 1,
                child: const Text(
                  'Confirm change',
                  style: kHouseTitleStyle,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
