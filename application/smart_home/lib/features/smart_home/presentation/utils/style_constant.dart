import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_home/core/constants/color_const.dart';

final kSensorDetailTitleStyle = TextStyle(
  fontFamily: GoogleFonts.electrolize().fontFamily,
  color: ColorConst.tealEnhanced,
  fontWeight: FontWeight.w700,
  fontSize: 24.sp,
  letterSpacing: 1.8.sp,
);

final kSensorDetailStyle = TextStyle(
  fontFamily: GoogleFonts.electrolize().fontFamily,
  color: ColorConst.tealEnhanced,
  fontWeight: FontWeight.w500,
  fontSize: 18.sp,
);

const kHouseTitleStyle = TextStyle(
  color: ColorConst.tealEnhanced,
  fontWeight: FontWeight.bold,
);

final kHouseDetailsStyle = TextStyle(
  color: ColorConst.tealEnhanced,
  fontWeight: FontWeight.w600,
  fontSize: 17.sp,
);

final kDetailsRowStyle = TextStyle(
  color: ColorConst.tealEnhanced,
  fontWeight: FontWeight.w400,
  fontSize: 15.sp,
);

final kDashboardTitleStyle = TextStyle(
  fontFamily: GoogleFonts.electrolize().fontFamily,
  color: ColorConst.tealEnhanced,
  fontWeight: FontWeight.w500,
  fontSize: 24.sp,
);

final kSpacer = SizedBox(height: 8.h);

final kPaddingHorizontal = 12.w;

final kPaddingVertical = 6.h;

final kHouseTileMargin = EdgeInsets.symmetric(
  horizontal: kPaddingHorizontal,
  vertical: kPaddingVertical,
);

final kHouseTileTitleStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 18.sp,
);

final kHouseTileSubTitleStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w300,
  fontSize: 16.sp,
);

final kLedTileMargin = EdgeInsets.symmetric(
  horizontal: kPaddingHorizontal,
  vertical: kPaddingVertical,
);

final kLedTileTitleStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 18.sp,
);

final kLedTileSubTitleStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w300,
  fontSize: 16.sp,
);
