import 'package:flutter/material.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';
import 'style_constant.dart';

Text? getDataAsText(DeviceData data, DeviceType type) {
  switch (type) {
    case DeviceType.thermometer:
      Color color;
      int? temp = data.temp;
      if (temp == null) {
        color = ColorConst.tealEnhanced;
      } else if (temp < 10) {
        color = Colors.blue;
      } else if (temp < 25) {
        color = Colors.green;
      } else if (temp < 35) {
        color = Colors.amber;
      } else {
        color = Colors.red;
      }
      return Text(
        temp != null ? '${data.temp} ºC' : 'No Data',
        style: kSensorDetailStyle.copyWith(
          color: color,
          fontWeight: FontWeight.bold,
        ),
      );
    case DeviceType.humidity:
      return Text(
        data.hum != null ? '${data.hum} %' : 'No Data',
        style: kSensorDetailStyle.copyWith(
          fontWeight: FontWeight.bold,
        ),
      );
    case DeviceType.gasConcentration:
      Color color;
      double? gasConcentration = data.gasConcentration;
      if (gasConcentration == null) {
        color = ColorConst.tealEnhanced;
      } else if (gasConcentration < 5) {
        color = Colors.green;
      } else if (gasConcentration < 10) {
        color = Colors.amber;
      } else {
        color = Colors.red;
      }
      return Text(
        data.gasConcentration != null ? '$gasConcentration %' : 'No Data',
        style: kSensorDetailStyle.copyWith(
          color: color,
          fontWeight: FontWeight.bold,
        ),
      );
    default:
      break;
  }
}
