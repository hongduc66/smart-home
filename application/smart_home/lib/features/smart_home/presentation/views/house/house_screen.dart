import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:smart_home/core/common_widgets/circular_icon_button.dart';
import 'package:smart_home/core/common_widgets/widget_button.dart';
import 'package:smart_home/features/smart_home/bloc/devices/house_remote_bloc.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';
import 'package:smart_home/features/smart_home/domain/entities/house.dart';
import 'package:smart_home/features/smart_home/presentation/components/air_conditioner.dart';
import 'package:smart_home/features/smart_home/presentation/components/door_tile.dart';
import 'package:smart_home/features/smart_home/presentation/components/house_details.dart';
import 'package:smart_home/features/smart_home/presentation/components/led_tile.dart';
import 'package:smart_home/features/smart_home/presentation/utils/helper_functions.dart';

import '../../utils/style_constant.dart';

class HouseScreen extends StatefulWidget {
  const HouseScreen({
    Key? key,
    required this.house,
  }) : super(key: key);
  final House house;

  @override
  _HouseScreenState createState() => _HouseScreenState();
}

class _HouseScreenState extends State<HouseScreen> {
  late final RefreshController _refreshController;
  late List<Device> listThermometers;
  late List<Device> listHumidity;
  late List<Device> listGasConcentration;
  late List<Device> listLeds;
  late List<Device> listDoors;

  @override
  void initState() {
    _refreshController = RefreshController();
    listThermometers = widget.house.sensors
        .where((i) => i.type == DeviceTypeName.thermometer)
        .toList();
    listHumidity = widget.house.sensors
        .where((i) => i.type == DeviceTypeName.humidity)
        .toList();
    listGasConcentration = widget.house.sensors
        .where((i) => i.type == DeviceTypeName.gasConcentration)
        .toList();
    listLeds = widget.house.sensors
        .where((i) => i.type == DeviceTypeName.led)
        .toList();
    listDoors = widget.house.sensors
        .where((i) => i.type == DeviceTypeName.door)
        .toList();
    BlocProvider.of<HouseRemoteBloc>(context).add(
      FetchHouseData(
        houseId: widget.house.id,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            header(),
            Expanded(child: houseDashboard()),
          ],
        ),
      ),
    );
  }

  Widget houseDashboard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: SmartRefresher(
        controller: _refreshController,
        onRefresh: () {
          BlocProvider.of<HouseRemoteBloc>(context).add(
            FetchHouseData(houseId: widget.house.id),
          );
          _refreshController.refreshCompleted();
        },
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            kSpacer,
            Text(
              'Sensors',
              style: kDashboardTitleStyle,
            ),
            kSpacer,
            Align(
              alignment: Alignment.center,
              child: SizedBox(
                width: 0.9.sw,
                height: 0.9.sw,
                child: sensorDetails(),
              ),
            ),
            kSpacer,
            Text(
              'Remote Led (${listLeds.length})',
              style: kDashboardTitleStyle,
            ),
            kSpacer,
            Align(
              alignment: Alignment.center,
              child: SizedBox(
                width: 0.95.sw,
                height: 115.h,
                child: ledList(),
              ),
            ),
            kSpacer,
            Text(
              'Security Auto Door (${listDoors.length})',
              style: kDashboardTitleStyle,
            ),
            kSpacer,
            Align(
              alignment: Alignment.center,
              child: SizedBox(
                width: 0.95.sw,
                height: 115.h,
                child: doorList(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget header() {
    return PhysicalModel(
      color: Colors.white,
      elevation: 2.h,
      child: Padding(
        padding: EdgeInsets.all(12.r),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CircularIconButtonSH(
              icon: Icons.arrow_back_sharp,
              onTap: () => Navigator.pop(context),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.r),
                child: SizedBox(
                  height: 24.h,
                  child: FittedBox(
                    child: Hero(
                      tag: 'house_name_${widget.house.id}',
                      child: Material(
                        type: MaterialType.transparency,
                        child: Text(
                          widget.house.name,
                          style: kHouseTitleStyle,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            CircularIconButtonSH(
              icon: Icons.feed,
              onTap: () => showHouseDetails(
                context: context,
                house: widget.house,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget sensorDetails() {
    return PageView(
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      children: [
        sensorsDetailPage(
          title: 'Thermometers (${listThermometers.length})',
          imageUrl: 'lib/asset/thermometer_background.jpg',
          type: DeviceType.thermometer,
        ),
        sensorsDetailPage(
          title: 'Humidity Sensors (${listHumidity.length})',
          imageUrl: 'lib/asset/humidity_background.jpg',
          type: DeviceType.humidity,
        ),
        sensorsDetailPage(
          title: 'Gas Sensors (${listGasConcentration.length})',
          imageUrl: 'lib/asset/gas_concentration_background.jpg',
          type: DeviceType.gasConcentration,
        ),
      ],
    );
  }

  Widget sensorsDetailPage({
    required String title,
    required String imageUrl,
    required DeviceType type,
  }) {
    List<Device>? sensors;
    List<DeviceData?>? sensorsData;

    return Padding(
      padding: EdgeInsets.all(4.r),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16.sp),
        child: Stack(
          children: [
            Opacity(
              opacity: 0.5,
              child: Image.asset(
                imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
              top: 15.h,
              left: 20.w,
              child: Text(
                title,
                style: kSensorDetailTitleStyle,
              ),
            ),
            Positioned(
              top: 60.h,
              left: 30.h,
              right: 30.h,
              bottom: 60.h,
              child: BlocBuilder<HouseRemoteBloc, HouseRemoteState>(
                builder: (context, state) {
                  if (state.status == HouseRemoteStatus.fetching) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (state.status != HouseRemoteStatus.fetchSuccess) {
                    return Center(
                      child: Text(
                        'Error occurs, refresh!',
                        style: kSensorDetailStyle,
                      ),
                    );
                  } else {
                    switch (type) {
                      case DeviceType.thermometer:
                        sensors = listThermometers;
                        sensorsData = state.sensorsData!['THERMOMETER'];
                        break;
                      case DeviceType.humidity:
                        sensors = listHumidity;
                        sensorsData = state.sensorsData!['HUMIDITY'];
                        break;
                      case DeviceType.gasConcentration:
                        sensors = listGasConcentration;
                        sensorsData = state.sensorsData!['GAS_CONCENTRATION'];
                        break;
                      default:
                        break;
                    }

                    return ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      itemCount: sensors!.length,
                      itemBuilder: (context, index) {
                        Device device = sensors![index];
                        return Wrap(
                          alignment: WrapAlignment.spaceBetween,
                          spacing: 10.w,
                          children: [
                            Text(
                              '${device.name} #${device.idx}',
                              style: kSensorDetailStyle,
                            ),
                            getDataAsText(
                              sensorsData!.firstWhere(
                                (data) => data?.houseSensorId == device.idx,
                                orElse: () => const DeviceData(),
                              )!,
                              type,
                            )!,
                          ],
                        );
                      },
                    );
                  }
                },
              ),
            ),
            type == DeviceType.thermometer && listThermometers.isNotEmpty
                ? Positioned(
                    right: 15.h,
                    bottom: 10.h,
                    child: WidgetButtonSH(
                      child: Text(
                        'Air Conditioner',
                        style: kHouseDetailsStyle,
                      ),
                      height: 40.h,
                      onTap: () => showAirConditionerRemote(
                        context,
                        listThermometers,
                        sensorsData,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  Widget ledList() {
    return listLeds.isEmpty
        ? Padding(
            padding: EdgeInsets.symmetric(vertical: 12.h),
            child: Text(
              'You have no Smart Led !',
              style: TextStyle(
                fontSize: 20.sp,
                fontWeight: FontWeight.w400,
                color: Colors.grey.shade500,
              ),
              textAlign: TextAlign.center,
            ),
          )
        : PageView.builder(
            physics: const BouncingScrollPhysics(),
            itemCount: listLeds.length,
            itemBuilder: (context, index) => LedTile(
              led: listLeds[index],
            ),
          );
  }

  Widget doorList() {
    return listDoors.isEmpty
        ? Padding(
            padding: EdgeInsets.symmetric(vertical: 12.h),
            child: Text(
              'You have no Smart Door !',
              style: TextStyle(
                fontSize: 20.sp,
                fontWeight: FontWeight.w400,
                color: Colors.grey.shade500,
              ),
              textAlign: TextAlign.center,
            ),
          )
        : PageView.builder(
            physics: const BouncingScrollPhysics(),
            itemCount: listDoors.length,
            itemBuilder: (context, index) => DoorTile(
              door: listDoors[index],
            ),
          );
  }
}
