import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smart_home/core/common_widgets/circular_icon_button.dart';
import 'package:smart_home/core/common_widgets/dropdown_button.dart';
import 'package:smart_home/core/common_widgets/text_form_field.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/core/constants/list_device_model.dart';
import 'package:smart_home/core/utils/snackbar.dart';
import 'package:smart_home/core/utils/validator.dart';
import 'package:smart_home/features/smart_home/bloc/house/house_manage_bloc.dart';

import '../../../../../router.dart';

class HouseRegisterForm extends StatefulWidget {
  const HouseRegisterForm({Key? key}) : super(key: key);

  @override
  _HouseRegisterFormState createState() => _HouseRegisterFormState();
}

class _HouseRegisterFormState extends State<HouseRegisterForm> {
  final _formKey = GlobalKey<FormState>();
  late List<Widget> _listInputRows;
  late List<Map<String, dynamic>> _devicesToJson;
  String houseName = '';

  @override
  void initState() {
    _listInputRows = [];
    _devicesToJson = [];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 0.2.sh,
              child: Center(child: _header()),
            ),
            _inputHouseName(),
            Expanded(
              child: _listInputRows.isNotEmpty
                  ? Form(
                      key: _formKey,
                      child: SingleChildScrollView(
                        child: Column(
                          children: _listInputRows,
                        ),
                      ),
                    )
                  : Center(
                      child: Text(
                        "To add a device type\nTap '+' button",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20.sp,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey.shade500,
                        ),
                      ),
                    ),
            ),
            SizedBox(
              height: 0.15.sh,
              child: _buttonBar(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _header() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Register House',
          style: TextStyle(
            fontSize: 36.sp,
            fontWeight: FontWeight.bold,
            color: ColorConst.tealEnhanced,
          ),
        ),
        SizedBox(height: 5.h),
        Text(
          'Ready for a smart house set-up !',
          style: TextStyle(
            fontSize: 16.sp,
            fontWeight: FontWeight.w300,
            color: ColorConst.tealEnhanced,
          ),
        ),
      ],
    );
  }

  Widget _inputHouseName() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 20.w),
      child: TextFormFieldSH(
        labelText: 'House',
        labelBehavior: FloatingLabelBehavior.always,
        hintText: 'Name your house...',
        prefixIcon: Icon(
          Icons.apartment_rounded,
          size: 24.sp,
          color: ColorConst.tealEnhanced,
        ),
        onChanged: (value) => houseName = value!,
        validator: (value) {
          if (!Validator.isValidNormalName(value!)) {
            return 'Invalid house name!';
          }
        },
      ),
    );
  }

  Widget _inputRow(int index) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 20.w),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: DropdownButtonSH(
              label: 'Device',
              listDropdownItems: listDeviceModelDropdownItem,
              onChanged: (value) => {
                _devicesToJson[index]['id'] = value,
              },
              validator: (value) {
                for (int i = 0; i < _devicesToJson.length; i++) {
                  if (value == _devicesToJson[i]['id'] &&
                      (value != null) &&
                      i != index) {
                    return 'Dupicate device ID: ${_devicesToJson[i]['id']}';
                  }
                }
              },
            ),
          ),
          SizedBox(width: 10.w),
          Expanded(
            flex: 2,
            child: TextFormFieldSH(
              labelText: 'Quantity',
              labelBehavior: FloatingLabelBehavior.always,
              hintText: '1 to 9',
              prefixIcon: Icon(
                Icons.layers_rounded,
                size: 24.sp,
                color: ColorConst.tealEnhanced,
              ),
              onChanged: (value) => {
                _devicesToJson[index]['quantity'] = value,
              },
              validator: (value) {
                if (Validator.isNormalNumber(value) == -1) {
                  return 'Invalid quantity';
                } else if (Validator.isNormalNumber(value) > 9) {
                  return 'Too much!';
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buttonBar() {
    return BlocListener<HouseManageBloc, HouseManageState>(
      listener: (context, state) {
        switch (state.status) {
          case HouseManageStatus.registerSuccess:
            showSnackbarSH(
                context, 'Register house successful!', AlertType.success);
            Navigator.pushNamedAndRemoveUntil(
              context,
              AppRouter.homePage,
              (route) => false,
            );
            break;
          case HouseManageStatus.failed:
            showSnackbarSH(context, state.errorMessage!, AlertType.error);
            break;
          default:
            break;
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          CircularIconButtonSH(
            size: 24.r,
            onTap: () => Navigator.pop(context),
            icon: Icons.arrow_back_rounded,
          ),
          CircularIconButtonSH(
            size: 24.r,
            onTap: () => setState(() {
              _listInputRows.add(
                _inputRow(_listInputRows.length),
              );
              _devicesToJson.add({
                'id': null,
                'quantity': null,
              });
            }),
            icon: Icons.add_sharp,
          ),
          CircularIconButtonSH(
            size: 24.r,
            onTap: () => setState(() {
              _listInputRows.clear();
              _devicesToJson.clear();
            }),
            icon: Icons.restore_sharp,
          ),
          CircularIconButtonSH(
            size: 24.r,
            onTap: () {
              if (_formKey.currentState == null) {
                return showSnackbarSH(
                  context,
                  'Cannot add a house without devices!',
                  AlertType.warning,
                );
              }
              if (_formKey.currentState!.validate()) {
                if (Validator.isValidNormalName(houseName)) {
                  /// All passed
                  FocusScope.of(context).unfocus;
                  context.read<HouseManageBloc>().add(
                        RegisterHouse(
                          name: houseName,
                          devices: _devicesToJson,
                        ),
                      );
                } else {
                  {
                    showSnackbarSH(
                      context,
                      'Please name your house!',
                      AlertType.error,
                    );
                  }
                }
              } else {
                showSnackbarSH(
                  context,
                  'Invalid input, try again!',
                  AlertType.error,
                );
              }
            },
            icon: Icons.done_rounded,
          ),
        ],
      ),
    );
  }
}
