import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smart_home/core/constants/color_const.dart';

class PasswordFormFieldSH extends StatefulWidget {
  const PasswordFormFieldSH({
    Key? key,
    required this.labelText,
    required this.hintText,
    this.prefixIcon,
    this.suffixIcon,
    required this.onChanged,
    required this.validator,
  }) : super(key: key);

  final String labelText;
  final String hintText;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Function(String?) onChanged;
  final String? Function(String?) validator;

  @override
  _PasswordFormFieldSHState createState() => _PasswordFormFieldSHState();
}

class _PasswordFormFieldSHState extends State<PasswordFormFieldSH> {
  bool _isPasswordHidden = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onChanged: widget.onChanged,
      validator: widget.validator,
      obscureText: _isPasswordHidden,
      style: TextStyle(fontSize: 16.sp),
      decoration: InputDecoration(
        labelText: widget.labelText,
        hintText: widget.hintText,
        fillColor: Colors.grey.withOpacity(0.2),
        prefixIcon: widget.prefixIcon ?? Container(),
        suffixIcon: IconButton(
          icon: Icon(
            _isPasswordHidden ? Icons.visibility_off : Icons.visibility,
            color: ColorConst.tealEnhanced,
            size: 24.sp,
          ),
          onPressed: () =>
              setState(() => _isPasswordHidden = !_isPasswordHidden),
        ),
        isDense: true,
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.r),
          borderSide: BorderSide.none,
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 10.w,
          vertical: 10.h,
        ),
      ),
    );
    ;
  }
}
