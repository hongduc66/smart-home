import 'package:flutter/material.dart';
import 'package:smart_home/core/common_widgets/dropdown_button.dart';

/// Giải pháp: tạo một API fetch toàn bộ thông tin về loại thiết bị ở Backend
/// sau đó lưu vào list. Cập nhật mỗi lần truy cập register form
List<DropdownMenuItem<int>> listDeviceModelDropdownItem = [
  buildDropdownItem(value: 1, title: '1 - Philips LED'),
  buildDropdownItem(value: 2, title: '2 - Sony Dark LED'),
  buildDropdownItem(value: 3, title: '3 - DHT11 Humidity Sensor'),
  buildDropdownItem(value: 4, title: '4 - Mijia BT 2'),
  buildDropdownItem(value: 5, title: '5 - Ammonia Gas Sensor'),
  buildDropdownItem(value: 6, title: '6 - Legion Smart Door'),
];
