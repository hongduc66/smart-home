import 'package:smart_home/core/utils/api_env_config.dart';

class APIConst {
  static String get baseUrl => _baseUrl!;

  static String? _baseUrl;

  static void setBaseUrl(Environment env) {
    switch (env) {
      case Environment.local:
        _baseUrl = APIEnvConfig.baseUrlLocal;
        break;
      case Environment.production:
        _baseUrl = APIEnvConfig.baseUrlProduction;
        break;
    }
  }
}

enum Environment { local, production }
