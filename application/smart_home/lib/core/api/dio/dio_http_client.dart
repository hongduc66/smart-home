import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:smart_home/core/constants/api_const.dart';
import 'package:smart_home/features/authentication/data/shared_preferences/authentication_storage.dart';

class DioHttpClient {
  final Dio _dio = Dio();
  static const int timeout = 8000;
  bool lockInterceptorForTokenRefresh = false;

  DioHttpClient() {
    _dio.options
      ..baseUrl = APIConst.baseUrl
      ..responseType = ResponseType.json
      ..connectTimeout = timeout
      ..sendTimeout = timeout
      ..receiveTimeout = timeout;

    _dio.interceptors.add(
      InterceptorsWrapper(
        onError: (DioError error, ErrorInterceptorHandler handler) async {
          log('Dio Error:' + error.message);

          /// Handle refresh token: Try to authenticate current credentials
          if (error.response?.statusCode == 401 &&
              lockInterceptorForTokenRefresh == false) {
            RequestOptions requestOptions = error.requestOptions;
            try {
              lockInterceptorForTokenRefresh = true;
              log('*************\nRefresh Token\n*************');
              var accessResponse = await postMethod('/authenticate', data: {
                'username': AuthenticationStorage.username,
                'password': AuthenticationStorage.password,
              });
              if (accessResponse.data['accessToken'] != null) {
                final opts = Options(method: requestOptions.method);
                configBearerToken(accessResponse.data['accessToken']);

                final response = await _dio.request(
                  requestOptions.path,
                  options: opts,
                  cancelToken: requestOptions.cancelToken,
                  onReceiveProgress: requestOptions.onReceiveProgress,
                  data: requestOptions.data,
                  queryParameters: requestOptions.queryParameters,
                );

                lockInterceptorForTokenRefresh = false;
                handler.resolve(response);
              }
            } catch (err) {
              lockInterceptorForTokenRefresh = false;
              handler.next(error);
            }
          } else {
            lockInterceptorForTokenRefresh = false;
            handler.next(error);
          }
        },
        onRequest: (RequestOptions request, RequestInterceptorHandler handler) {
          log(request.method + ' ' + request.path);
          return handler.next(request);
        },
        onResponse: (Response response, ResponseInterceptorHandler handler) {
          return handler.next(response);
        },
      ),
    );

    if (!kReleaseMode) {
      _dio.interceptors.add(LogByMethodInterceptor(
        disabledLogMethods: [],
        disabledLogPaths: [],
      ));
    }
  }

  void _configRequestHeaders() {
    _dio.options.headers.addAll({
      "Accept": "application/json",
      "content-type": Headers.jsonContentType
    });

    final accessToken = AuthenticationStorage.token;
    if (accessToken != null) {
      _dio.options.headers["Authorization"] = "Bearer $accessToken";
    }
  }

  void configBearerToken(String accessToken) {
    AuthenticationStorage.updateToken(accessToken);
    _dio.options.headers['Authorization'] = "Bearer $accessToken";
  }

  Future<Response<T>> getMethod<T>(String uri,
      {required Map<String, dynamic> params}) async {
    _configRequestHeaders();
    return await _dio.get<T>(uri, queryParameters: params);
  }

  Future<Response<T>> postMethod<T>(String uri, {dynamic data}) async {
    _configRequestHeaders();
    return await _dio.post(uri, data: data);
  }

  Future<Response<T>> putMethod<T>(String uri, {dynamic data}) async {
    _configRequestHeaders();
    return await _dio.put(uri, data: data);
  }

  Future<Response<T>> deleteMethod<T>(String uri,
      {required Map<String, dynamic> params}) async {
    _configRequestHeaders();
    return await _dio.delete(uri, queryParameters: params);
  }
}

class LogByMethodInterceptor extends LogInterceptor {
  LogByMethodInterceptor(
      {this.disabledLogMethods = const [], this.disabledLogPaths = const []}) {
    requestHeader = true;
    requestBody = false;
    responseHeader = false;
    responseBody = true;
    logPrint = _logPrint;
  }

  List<String> disabledLogMethods;
  List<String> disabledLogPaths;

  void _logPrint(Object object) {
    try {
      var json = const JsonDecoder().convert(object.toString());
      JsonEncoder encoder = const JsonEncoder.withIndent('    ');
      String prettyLog = encoder.convert(json);
      log(prettyLog);
    } catch (e) {
      log(object as String);
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    if (disabledLogMethods
        .contains(response.requestOptions.method.toUpperCase())) {
      return null;
    }

    for (var path in disabledLogPaths) {
      if (response.requestOptions.path.startsWith(path)) {
        return null;
      }
    }

    return super.onResponse(response, handler);
  }
}
