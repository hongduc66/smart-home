import 'package:dio/dio.dart';

class ApiError {
  int? _code;
  String _message = "";

  int get code => _code!;
  String get message => _message;

  ApiError({dynamic error}) {
    if (error != null) _handleError(error);
  }

  factory ApiError.fromJson(Map<String, dynamic> json) {
    return ApiError()
      .._code = json['code'] as int
      .._message = json['message'] as String;
  }

  _handleError(dynamic error) {
    if (error is DioError) {
      _code = error.response?.statusCode ?? 0;
      switch (error.type) {
        case DioErrorType.connectTimeout:
        case DioErrorType.sendTimeout:
        case DioErrorType.receiveTimeout:
          _message = 'Connection timeout';
          break;
        default:
          _message = error.message;
          break;
      }
    } else {
      _message = error.toString();
    }
  }
}
