abstract class ApiResult<T> {
  T? data;
  ApiResult();
}

class ApiResultSingle<T> extends ApiResult<T> {
  ApiResultSingle({
    required Map<String, dynamic> json,
    required String rootName,
    required T Function(dynamic json) jsonConverter,
  }) {
    dynamic dataJson;
    if (rootName != '') {
      if (!json.containsKey(rootName)) return;
      dataJson = json[rootName];
    } else {
      dataJson = json;
    }

    data = jsonConverter(dataJson);
  }
}

class ApiResultAsList<T> extends ApiResult<List<T>> {
  ApiResultAsList({
    required List<dynamic> list,
    String? rootName,
    required T Function(dynamic json) jsonConverter,
  }) {
    data = list.map(jsonConverter).toList();
  }
}

class ApiResultAsListWithRootName<T> extends ApiResult<List<T>> {
  ApiResultAsListWithRootName({
    required Map<String, dynamic> json,
    required String rootName,
    required T Function(dynamic json) jsonConverter,
  }) {
    dynamic dataJson;
    if (rootName != '') {
      if (!json.containsKey(rootName)) return;
      dataJson = json[rootName];
    } else {
      dataJson = json;
    }

    if (dataJson is List) {
      data = dataJson.map(jsonConverter).toList();
    }
  }
}
