import 'dart:convert';
import 'dart:developer';

import 'package:dart_amqp/dart_amqp.dart';
import 'package:smart_home/features/smart_home/domain/entities/device.dart';

class AmqpClient {
  late final Client client;

  AmqpClient() {
    ConnectionSettings settings = ConnectionSettings(
      host: "localhost",
      port: 5672,
    );
    client = Client(settings: settings);
  }

  Future<bool> switchDevice({
    required Device device,
    required String fieldName,
    required var data,
  }) async {
    try {
      Channel channel = await client.channel();
      Queue queue = await channel.queue(
        'queue.hust.iot.action',
        durable: true,
      );
      Map<String, dynamic> requestData = {};
      requestData['houseSensorId'] = device.idx;
      requestData['sensorType'] = device.type;
      requestData[fieldName] = data;
      queue.publish(json.encode(requestData));
      return true; // As sending message successful
    } catch (exception) {
      log('Request failed: $exception');
      return false; // Sent failed
    }
  }
}
