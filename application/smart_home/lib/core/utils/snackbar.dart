import 'package:flutter/material.dart';

void showSnackbarSH(BuildContext context, String content, AlertType type) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      backgroundColor: Colors.black.withOpacity(0.5),
      elevation: 10.0,
      content: Row(
        children: [
          getIcon(type),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Text(
                content,
                style: TextStyle(
                  color: Colors.white.withOpacity(0.8),
                  fontFamily: Theme.of(context).textTheme.headline1!.fontFamily,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          //const Spacer(),
        ],
      ),
    ),
  );
}

Icon getIcon(AlertType type) {
  switch (type) {
    case AlertType.success:
      return const Icon(
        Icons.check_circle,
        color: Colors.green,
      );
    case AlertType.error:
      return const Icon(
        Icons.error,
        color: Colors.red,
      );
    case AlertType.warning:
      return const Icon(
        Icons.warning,
        color: Colors.yellow,
      );
    case AlertType.info:
      return const Icon(
        Icons.announcement,
        color: Colors.teal,
      );
  }
}

enum AlertType { success, error, warning, info }
