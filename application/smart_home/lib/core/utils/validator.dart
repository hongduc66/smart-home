class Validator {
  static final RegExp _emailRegExp = RegExp(
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
  );

  static final RegExp _passwordRegExp = RegExp(
    r'^.{4,8}$',
  );

  static final RegExp _usernameRegExp = RegExp(
    r'^(?=[a-zA-Z0-9._]{6,20}$)(?!.*[_.]{2})[^_.].*[^_.]$',
  );

  static final RegExp _normalNameRegExp = RegExp(
    r'^[^±!@£$%^&*_+§¡€#¢§¶•ªº«\\/<>?:;|=.,]{1,20}$',
  );

  static isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  static isValidPassword(String password) {
    return _passwordRegExp.hasMatch(password);
  }

  static isValidUsername(String username) {
    return _usernameRegExp.hasMatch(username);
  }

  static isValidNormalName(String name) {
    return _normalNameRegExp.hasMatch(name);
  }

  static isNormalNumber(String? id) {
    if (id == null) {
      return -1;
    } else {
      try {
        int? value = int.parse(id);
        if (value > 0) return value;
      } catch (exception) {
        return -1;
      }
    }
    return -1;
  }
}
