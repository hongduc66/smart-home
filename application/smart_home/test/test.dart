import 'package:smart_home/core/api/dio/dio_http_client.dart';
import 'package:smart_home/core/constants/api_const.dart';

void main() async {
  APIConst.setBaseUrl(Environment.local);
  DioHttpClient httpClient = DioHttpClient();
  var response = await httpClient.postMethod(
    '/authenticate',
    data: {
      "username": "hongduc666",
      "password": "1234",
    },
  );
  print(response.data);
}
