import "package:dart_amqp/dart_amqp.dart";

void main() async {
  Client client = Client();

  Channel channel = await client
      .channel(); // auto-connect to localhost:5672 using guest credentials
  Queue queue = await channel.queue('queue.hust.iot.aaa', durable: true);
  Consumer consumer = await queue.consume();
  consumer.listen((AmqpMessage message) {
    // Get the payload as a string
    print(" [x] Received string: ${message.payloadAsString}");

    // The message object contains helper methods for
    // replying, ack-ing and rejecting
  });

  queue.publish('Fuck you rabbit');
}
